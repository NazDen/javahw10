package happyfamily;

public class DomesticCat extends Pet implements foul{
    @Override
    void respond() {
        System.out.println("Мяу.");
    }

    @Override
    public void foul() {

            System.out.println("Нужно хорошо замести следы...");

    }

    public DomesticCat(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
        super.setSpecies(Species.DOMESTICCAT);
    }
}
