package happyfamily;


import java.util.Random;

public final class Women extends Human implements HumanCreator {

    public Women(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Women(String name, String surname, int year, int iq, String[][] schedule) {
        super(name, surname, year, iq, schedule);
    }

    public Women() {
    }

    @Override
    void greetPet() {
        System.out.println("Привет, "+ super.getFamily().getPet().getNickname()+".");
    }

    public void makeUp(){
        System.out.println("Надо накраситса, а то хожу как чучело.");
    }

    @Override
    public Human bornChild(int year) {
        String[] boyNames = {"Коля", "Антон", "Ростик", "Роман", "Владислав"};
        String[] girlNames = {"Наташа", "Ира", "Оксана", "Петунья", "Зоряна"};
        Random random= new Random();
        int sex= random.nextInt(2);

        if (sex==0){
            int index= random.nextInt(girlNames.length);
            Women girl = new Women();
            girl.setYear(year);
            girl.setFamily(super.getFamily());
            girl.setName(girlNames[index]);
            girl.setSurname(getFamily().getFather().getSurname());
            girl.setIq((getFamily().getFather().getIq()+ getFamily().getMother().getIq())/2);
            girl.getFamily().addChild(girl);
            return girl;
        }
        else{
            int index= random.nextInt(boyNames.length);
            Man boy = new Man();
            boy.setYear(year);
            boy.setFamily(super.getFamily());
            boy.setName(boyNames[index]);
            boy.setSurname(getFamily().getFather().getSurname());
            boy.setIq((getFamily().getFather().getIq()+ getFamily().getMother().getIq())/2);
            boy.getFamily().addChild(boy);
            return boy;
        }
    }
}
