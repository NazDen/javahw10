package happyfamily;

public final class Man extends Human {


    public Man(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Man(String name, String surname, int year, int iq, String[][] schedule) {
        super(name, surname, year, iq, schedule);
    }

    public Man() {
    }

    @Override
    void greetPet() {
        System.out.println("Хай, "+ super.getFamily().getPet().getNickname()+".");
    }

    public void repairCar(){
        System.out.println("Пойду ка я в гараж, посмотрю как моя ласточка пожываєт.");
    }
}
