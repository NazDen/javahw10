package happyfamily;

public class RoboCat extends Pet implements foul{
    @Override
    void respond() {
        System.out.println("Привет, хозяин. Я - "+this.getNickname()+". Я соскучился!");
    }

    @Override
    public void foul() {
        {
            System.out.println("Нужно хорошо замести следы...");
        }
    }

    public RoboCat(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
        super.setSpecies(Species.ROBOCAT);
    }
}
